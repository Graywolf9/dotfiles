#!/bin/bash

cd ~
#mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
mkdir -p ~/rpmbuild/BUILD
mkdir -p ~/rpmbuild/RPMS
mkdir -p ~/rpmbuild/SOURCES
mkdir -p ~/rpmbuild/SPECS
mkdir -p ~/rpmbuild/SRPMS
cp ~/files/dwm-6.4.tar.gz ~/rpmbuild/SOURCES
cp ~/files/dwm.spec ~/rpmbuild/SPECS
cd ~/rpmbuild
rpmbuild -v -bb --clean SPECS/dwm.spec
cp -R ~/rpmbuild/RPMS/* ~/files

tail -f /dev/null
