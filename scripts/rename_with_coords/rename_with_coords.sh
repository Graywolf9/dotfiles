for x in $(ls *.jpg); do 
	echo $x;
	LAT=$(exif $x | grep "Latitud  " | awk 'BEGIN { FS = "|" } ; { print $2 }' | tr "," " " | xargs python /Users/graywolf9/Desktop/coord/ccconvert.py)
	LON=$(exif $x | grep "Longitud  " | awk 'BEGIN { FS = "|" } ; { print $2 }' | tr "," " " | xargs python /Users/graywolf9/Desktop/coord/ccconvert.py)

	NUEVO=$LAT,-$LON
	echo $NUEVO

	mv $x $NUEVO.jpg
done
