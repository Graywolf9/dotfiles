tar -cf - $1 | pv -s $(($(du -sk $1 | awk '{print $1}') * 1024)) | gzip > $2
