! [ -d optimizado ] && mkdir optimizado

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile="optimizado/${1%.*}.pdf" "$1"
