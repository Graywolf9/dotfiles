podman-compose stop | xargs -I {} podman rm -f {}
podman images | grep $(basename $PWD) | awk '{ print $3 }' | xargs -I {} podman rmi -f {}
