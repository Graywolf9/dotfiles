"""
    apt install -y python3 python3-pip
    pip3 install pandas openpyxl
"""

import pandas as pd
import sys

df = pd.read_csv(sys.argv[1])

df.to_excel(sys.argv[1].replace("csv","xlsx"))
