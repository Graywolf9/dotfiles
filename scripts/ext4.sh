# Creates here a ext4 folder that is a mountpoint of a folder with this name on the ext4 mountpoint

D=$( [ $1 ] && echo "$1" || echo "ext4" )

DIR=$(basename $PWD)
EDIR="$HOME/ext4.d/$DIR"
DDIR="$PWD/$D"

echo $DIR
echo $EDIR
echo $DDIR

mkdir -p $EDIR
mkdir -p $DDIR
pkexec sh -c "mount -o bind $EDIR $DDIR"
