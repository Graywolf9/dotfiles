# bg_color
[ $2 ] || exit;
# width
[ $3 ] || exit;
# height
[ $4 ] || exit;
# orientation
O=$3x
[ $5 ] && O=x$4

convert -gravity center -size $3x$4 xc:$2 $1 -resize $O -composite "${1%.*}_resized.jpg"
