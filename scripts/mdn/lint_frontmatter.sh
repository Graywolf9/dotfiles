# Lint frontmatter
#cd /path/to/mdn/content
#yarn fix:fm --configFile /path/to/mdn/translated-content/front-matter-config.json /path/to/mdn/translated-content/files/<locale>
#cat ~/Desktop/ttt | xargs -I {} npx yarn fix:fm --configFile ../translated-content/front-matter-config.json ../translated-content/{}

# From translated-content
git ls-files -mo --exclude-standard | xargs -I {} node ../content/scripts/front-matter_linter.js --fix true --configFile front-matter-config.json {}

# Lint frontmatter (option 2)
# https://github.com/mdn/translated-content/issues/7412
# https://gist.github.com/YujiSoftware/bfa7aa0957d276c679f52d254baea6b1
#ruby remove_frontmatter_keys.rb files\ja\webassembly
