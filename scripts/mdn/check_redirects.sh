# Check redirects from yari run: yarn tool validate-redirects --strict

# Check
yarn content validate-redirects --strict

# Fix
yarn content fix-redirects es
