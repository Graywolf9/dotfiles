# Speed up video without audio
#ffmpeg -i $video -filter:v "setpts=0.5*PTS" -an ../testt/$video;
# Speed up audio withoud video
#ffmpeg -i $video -filter:a "atempo=2.0" -vn ../testt/$video.mp3;

mkdir x2
# Speed up video and audio
ffmpeg -i "$1" -filter_complex "[0:v]setpts=0.5*PTS[v];[0:a]atempo=2.0[a]" -map "[v]" -map "[a]" x2/"${1%.*}.mp4"
