[ -d neofetch ] || git clone https://github.com/dylanaraps/neofetch
cd neofetch
sudo make install
cd -
echo 'alias neofetch="neofetch --w3m --source ~/distro.png --image_size none"' >> ~/.bashrc
