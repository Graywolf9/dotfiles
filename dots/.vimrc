" Settings
:set mouse=a
:set nu
":set tabstop=2
":set expandtab
:set shiftwidth=2 smarttab

" Install plugin manager
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
call plug#begin('~/.vim/plugged')

  Plug 'preservim/nerdtree'
  Plug 'mattn/emmet-vim'
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'junegunn/fzf'
  Plug 'junegunn/fzf.vim'

call plug#end()
" Install plugins
" :source ~/.vimrc
" :PlugInstall

" PluginSettings
let g:NERDTreeMouseMode=3
let g:user_emmet_leader_key=','
let g:coc_disable_startup_warning = 1

" Mapping
nnoremap <silent> <C-D> :NERDTreeToggle<CR>
inoremap <silent> <C-D> <Esc>:NERDTreeToggle<CR>
nnoremap <silent> <C-A> :Files<CR>
inoremap <silent> <C-A> <Esc>:Files<CR>
" nnoremap <silent> <C-W> :w<CR>
" inoremap <silent> <C-W> <Esc>:w<CR>A

" use <tab> to trigger completion and navigate to the next complete item COC
function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <silent><expr> <Tab>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()

nmap <silent> gd <Plug>(coc-definition)

" Python compile
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python' shellescape(@%, 1)<CR>

" Java compile
autocmd FileType java map <buffer> <F9> :w<CR>:exec '!javac' shellescape(@%, 1)<CR>
autocmd FileType java imap <buffer> <F9> <esc>:w<CR>:exec '!javac' shellescape(@%, 1)<CR>

" PHP compile
autocmd FileType php map <buffer> <F9> :w<CR>:exec '!php' shellescape(@%, 1)<CR>
autocmd FileType php imap <buffer> <F9> <esc>:w<CR>:exec '!php' shellescape(@%, 1)<CR>

" Node compile
autocmd FileType javascript map <buffer> <F9> :w<CR>:exec '!node' shellescape(@%, 1)<CR>
autocmd FileType javascript imap <buffer> <F9> <esc>:w<CR>:exec '!node' shellescape(@%, 1)<CR>
