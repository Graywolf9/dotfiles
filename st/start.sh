#!/bin/bash

cd ~
#mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
mkdir -p ~/rpmbuild/BUILD
mkdir -p ~/rpmbuild/RPMS
mkdir -p ~/rpmbuild/SOURCES
mkdir -p ~/rpmbuild/SPECS
mkdir -p ~/rpmbuild/SRPMS
cp ~/files/st-0.9.tar.gz ~/rpmbuild/SOURCES
cp ~/files/st.spec ~/rpmbuild/SPECS
cd ~/rpmbuild
rpmbuild -v -bb --clean SPECS/st.spec
cp -R ~/rpmbuild/RPMS/* ~/files
