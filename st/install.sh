[ -d ./st/ ] || git clone https://git.suckless.org/st
cd st
curl -O http://st.suckless.org/patches/dracula/st-dracula-0.8.5.diff
patch -p1 < st-dracula-0.8.5.diff
#sudo cp ./theme/fonts/SourceCodePro+Powerline+Awesome+Regular.ttf /usr/local/share/fonts/
cd ..

tar -czf st-0.9.tar.gz st

podman run -ti --rm -v $PWD:/root/files $(podman build -q .)
